package com.example.gratters

import android.app.Application
import com.example.gratters.di.component.AppComponent
import com.example.gratters.di.component.DaggerAppComponent
import com.example.gratters.di.module.AppModule

/**
 * Класс приложения
 * @author StrangeHare
 * Date: 20.09.17
 */
class App : Application() {

    companion object {
        /** Компонент */
        lateinit var component: AppComponent
        /** Инстанс приложения */
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        component = DaggerAppComponent
                .builder()
                .appModule(AppModule(instance))
                .build()
    }
}