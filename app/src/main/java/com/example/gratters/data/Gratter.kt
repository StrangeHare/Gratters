package com.example.gratters.data

/**
 * Класс модели поздравления
 * @author StrangeHare
 *         Date: 22.09.17
 */
data class Gratter(val text: String,
                   val author: String)