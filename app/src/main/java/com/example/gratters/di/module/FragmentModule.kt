package com.example.gratters.di.module

import android.support.v4.app.Fragment
import com.example.gratters.di.qualifier.FragmentChildFragmentManager
import com.example.gratters.di.scope.FragmentScope
import dagger.Module
import dagger.Provides

/**
 * Модуль фрагмента
 * @author StrangeHare
 *         Date: 21.09.17
 */
@Module
class FragmentModule(private var fragment: Fragment) {

    /**
     * Провайдим фрагмент менеджер
     * @return менеджер
     */
    @Provides
    @FragmentScope
    @FragmentChildFragmentManager
    fun provideChildeFragmentManager() = fragment.childFragmentManager
}