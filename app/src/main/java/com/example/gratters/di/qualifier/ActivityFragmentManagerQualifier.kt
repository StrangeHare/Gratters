package com.example.gratters.di.qualifier

import javax.inject.Qualifier

/**
 * Квалификатор менеджера фрагментов полученного из активности
 * @author StrangeHare
 *         Date: 20.09.17
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityFragmentManagerQualifier