package com.example.gratters.di.module

import android.content.Context
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.example.gratters.di.qualifier.ActivityFragmentManagerQualifier
import com.example.gratters.di.qualifier.ActivityContextQualifier
import com.example.gratters.di.scope.ActivityScope
import com.example.gratters.ui.base.navigator.ActivityNavigator
import com.example.gratters.ui.base.navigator.INavigator
import dagger.Module
import dagger.Provides

/**
 * Модуль активности
 * @author StrangeHare
 *         Date: 20.09.17
 */
@Module
class ActivityModule(private var activity: AppCompatActivity) {

    /**
     * Предоставление контекста активности
     * @return контекст
     */
    @Provides
    @ActivityScope
    @ActivityContextQualifier
    fun provideActivityContext(): Context = activity

    /**
     * Предоставление Навигатора
     * @return навигатор
     */
    @Provides
    @ActivityScope
    fun provideNavigator(): INavigator = ActivityNavigator(activity)

    /**
     * Предоставление менеджера
     * @return менеджер
     */
    @Provides
    @ActivityScope
    @ActivityFragmentManagerQualifier
    fun provideFragmentManager(): FragmentManager = activity.supportFragmentManager
}