package com.example.gratters.di.component

import com.example.gratters.di.module.ViewHolderModule
import com.example.gratters.di.scope.ViewHolderScope
import com.example.gratters.ui.main.recyclerview.GrattersViewHolder
import dagger.Component

/**
 * Компонент вью холдера
 * @author StrangeHare
 *         Date: 22.09.17
 */
@ViewHolderScope
@Component(modules = arrayOf(ViewHolderModule::class))
interface ViewHolderComponent {

    /**
     * Инжект холдера
     * @param holder холдер
     */
    fun injectGrattersViewHolder(holder: GrattersViewHolder)
}