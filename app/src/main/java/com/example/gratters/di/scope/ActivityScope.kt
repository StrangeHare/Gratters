package com.example.gratters.di.scope

import javax.inject.Scope

/**
 * Аннотация жизни активности
 * @author StrangeHare
 *         Date: 20.09.17
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope