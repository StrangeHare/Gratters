package com.example.gratters.di.scope

import javax.inject.Scope

/**
 * Аннотация жизни фрагмента
 * @author StrangeHare
 *         Date: 21.09.17
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope