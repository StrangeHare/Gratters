package com.example.gratters.di.module

import dagger.Module

/**
 * Модуль вью холдера
 * @author StrangeHare
 *         Date: 22.09.17
 */
@Module
class ViewHolderModule