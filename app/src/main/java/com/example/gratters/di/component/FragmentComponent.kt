package com.example.gratters.di.component

import com.example.gratters.di.module.FragmentModule
import com.example.gratters.di.module.ViewModelModule
import com.example.gratters.di.scope.FragmentScope
import com.example.gratters.ui.main.viewpager.pagerfragments.favorite.FavoriteGrattersFragment
import com.example.gratters.ui.main.viewpager.pagerfragments.all.AllGrattersFragment
import dagger.Component

/**
 * Компонент фрагмента
 * @author StrangeHare
 *         Date: 21.09.17
 */
@FragmentScope
@Component(dependencies = arrayOf(ActivityComponent::class), modules = arrayOf(FragmentModule::class, ViewModelModule::class))
interface FragmentComponent {

    /**
     * Инжект фрагманта
     * @param fragment
     */
    fun inject(fragment: AllGrattersFragment)

    /**
     * Инжект фрагманта
     * @param fragment
     */
    fun inject(fragment: FavoriteGrattersFragment)
}