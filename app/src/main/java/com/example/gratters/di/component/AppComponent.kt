package com.example.gratters.di.component

import android.content.Context
import com.example.gratters.di.module.AppModule
import com.example.gratters.di.qualifier.AppContextQualifier
import com.example.gratters.di.scope.AppScope
import dagger.Component

/**
 * Компонент приложения
 * @author StrangeHare
 *         Date: 20.09.17
 */
@AppScope
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    /**
     * Получение контекста приложения
     * @return контекст
     */
    @AppContextQualifier
    fun getAppContext(): Context
}