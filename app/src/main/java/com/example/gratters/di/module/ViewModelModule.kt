package com.example.gratters.di.module

import com.example.gratters.ui.main.viewpager.pagerfragments.all.AllGrattersViewModel
import com.example.gratters.ui.main.viewpager.pagerfragments.all.IAllGrattersViewModel
import com.example.gratters.ui.main.viewpager.pagerfragments.favorite.FavoriteGrattersViewModel
import com.example.gratters.ui.main.viewpager.pagerfragments.favorite.IFavoriteGrattersViewModel
import dagger.Binds
import dagger.Module

/**
 * Модуль вью холдера
 * @author StrangeHare
 *         Date: 22.09.17
 */
@Module
abstract class ViewModelModule {

    /**
     * Получение вью модели
     * @param viewModel вью модель
     * @return реализация
     */
    @Binds
    abstract fun provideAllGrattersViewModel(viewModel: AllGrattersViewModel) : IAllGrattersViewModel

    /**
     * Получение вью модели
     * @param viewModel вью модель
     * @return реализация
     */
    @Binds
    abstract fun provideFavoriteGrattersViewModel(viewModel: FavoriteGrattersViewModel) : IFavoriteGrattersViewModel
}
