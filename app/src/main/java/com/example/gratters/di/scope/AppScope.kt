package com.example.gratters.di.scope

import javax.inject.Scope

/**
 * Аннотация жизни приложения
 * @author StrangeHare
 *         Date: 20.09.17
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope