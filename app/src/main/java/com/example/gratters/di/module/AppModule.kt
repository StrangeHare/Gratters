package com.example.gratters.di.module

import android.app.Application
import android.content.Context
import com.example.gratters.di.qualifier.AppContextQualifier
import com.example.gratters.di.scope.AppScope
import dagger.Module
import dagger.Provides

/**
 * Модуль приложения
 * @author StrangeHare
 *         Date: 20.09.17
 */
@Module
class AppModule(var app: Application) {

    /**
     * Предоставление контекста приложения
     * @return контекст
     */
    @AppScope
    @AppContextQualifier
    @Provides
    fun provideAppContext(): Context = app
}