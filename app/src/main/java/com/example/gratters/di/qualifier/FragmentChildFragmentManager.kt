package com.example.gratters.di.qualifier

import javax.inject.Qualifier

/**
 * Квалификатор менеджера
 * @author StrangeHare
 *         Date: 22.09.17
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentChildFragmentManager