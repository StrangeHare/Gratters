package com.example.gratters.di.scope

import javax.inject.Scope

/**
 * Аннотация жизни холдера
 * @author StrangeHare
 *         Date: 22.09.17
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ViewHolderScope