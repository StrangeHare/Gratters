package com.example.gratters.di.component

import android.content.Context
import android.support.v4.app.FragmentManager
import com.example.gratters.di.module.ActivityModule
import com.example.gratters.di.module.ViewModelModule
import com.example.gratters.di.qualifier.ActivityFragmentManagerQualifier
import com.example.gratters.di.qualifier.ActivityContextQualifier
import com.example.gratters.di.scope.ActivityScope
import com.example.gratters.ui.base.navigator.INavigator
import com.example.gratters.ui.main.MainActivity
import dagger.Component

/**
 * Компонент активности
 * @author StrangeHare
 *         Date: 20.09.17
 */
@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(ActivityModule::class, ViewModelModule::class))
interface ActivityComponent {

    /**
     * Получить контекст активности
     * @return контекст
     */
    @ActivityContextQualifier
    fun getActivityContext(): Context

    /**
     * Получение менеджера
     * @return менеджер
     */
    @ActivityFragmentManagerQualifier
    fun getFragmentManager(): FragmentManager

    /**
     * Получение навигатора
     * @return навигатор
     */
    fun getNavigator(): INavigator

    /**
     * Вводим главную активность
     * @param activity активность
     */
    fun injectMainActivity(activity: MainActivity)
}