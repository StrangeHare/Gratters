package com.example.gratters.ui.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import com.example.gratters.App
import com.example.gratters.di.component.ActivityComponent
import com.example.gratters.di.component.DaggerActivityComponent
import com.example.gratters.di.module.ActivityModule
import com.example.gratters.ui.base.viewmodel.IMvvmViewModel
import com.example.gratters.ui.main.viewpager.IGrattersView

/**
 * Базовая активность
 * @author StrangeHare
 *         Date: 20.09.17
 */
abstract class BaseActivity<B : ViewDataBinding, V : IMvvmViewModel<IGrattersView>> : AppCompatActivity() {

    /** Компонент активити */
    private var activityComponent: ActivityComponent? = null
    /** Биндинг */
    var binding: B? = null

    /**
     * Получение компонента активити
     * @return компонент активити
     */
    fun getActivityComponent(): ActivityComponent? {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(ActivityModule(this))
                    .appComponent(App.component)
                    .build()
        }

        return activityComponent
    }

    /**
     * Инициализация биндинга
     * @param layoutResID ид макета
     */
    protected fun setAndBindContentView(@LayoutRes layoutResID: Int, savedInstanceState: Bundle?) {
        binding = DataBindingUtil.setContentView(this, layoutResID)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
        activityComponent = null
    }
}