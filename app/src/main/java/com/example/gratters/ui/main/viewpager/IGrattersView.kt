package com.example.gratters.ui.main.viewpager

import com.example.gratters.ui.base.view.IMvvmView

/**
 * Интерфейс вью
 * @author StrangeHare
 *         Date: 22.09.17
 */
interface IGrattersView : IMvvmView {

    /**
     * Обновление
     * @code {success true обновлние прошло успешно}
     */
    fun onRefresh(success: Boolean)
}