package com.example.gratters.ui.base.navigator

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment

/**
 * Активити Навигатор
 * @author StrangeHare
 *         Date: 20.09.17
 */
class ActivityNavigator(private var activity: Activity) : INavigator {

    override fun finishActivity() {
        activity.finish()
    }

    override fun startActivity(intent: Intent) {
        activity.startActivity(intent)
    }

    override fun startActivity(action: String) {
        activity.startActivity(Intent(action))
    }

    override fun startActivity(action: String, uri: Uri) {
        activity.startActivity(Intent(action, uri))
    }

    override fun startActivity(activityClass: Class<out Activity>) {
    }

    override fun startActivity(activityClass: Class<out Activity>, args: Bundle) {
    }

    override fun startActivity(activityClass: Class<out Activity>, args: Parcelable) {
    }

    override fun startActivity(activityClass: Class<out Activity>, arg: String) {
    }

    override fun startActivity(activityClass: Class<out Activity>, arg: Int) {
    }

    override fun startActivityForResult(activityClass: Class<out Activity>, requestCode: Int) {
    }

    override fun startActivityForResult(activityClass: Class<out Activity>, arg: Parcelable, requestCode: Int) {
    }

    override fun startActivityForResult(activityClass: Class<out Activity>, arg: String, requestCode: Int) {
    }

    override fun startActivityForResult(activityClass: Class<out Activity>, arg: Int, requestCode: Int) {
    }

    override fun replaceFragment(containerId: Int, fragment: Fragment, args: Bundle) {
    }

    override fun replaceFragment(containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle) {
    }

    override fun replaceFragmentAndAddToBackStack(containerId: Int, fragment: Fragment, args: Bundle, backstackTag: String) {
    }

    override fun replaceFragmentAndAddToBackStack(containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle, backstackTag: String) {
    }
}