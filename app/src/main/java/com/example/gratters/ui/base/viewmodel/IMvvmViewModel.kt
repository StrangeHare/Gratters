package com.example.gratters.ui.base.viewmodel

import android.os.Bundle
import com.example.gratters.ui.base.view.IMvvmView

/**
 * Интерфейс вью модели
 * @author StrangeHare
 *         Date: 20.09.17
 */
interface IMvvmViewModel<in V : IMvvmView> {

    /**
     * Присоединение вью
     * @param view               вью
     * @param savedInstanceState бандл
     */
    fun attachView(view: V, savedInstanceState: Bundle?)

    /**
     * Отсоединение вью
     */
    fun detachView()

    /**
     * Сохранение стейта
     * @param outState бандл
     */
    fun saveInstanceState(outState: Bundle?)
}