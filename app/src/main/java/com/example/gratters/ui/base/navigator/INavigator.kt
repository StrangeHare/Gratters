package com.example.gratters.ui.base.navigator

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.IdRes
import android.support.v4.app.Fragment

/**
 * Навигатор
 * @author StrangeHare
 *         Date: 20.09.17
 */
interface INavigator {

    /** Завершить активити */
    fun finishActivity()

    /** Старт Активити */
    fun startActivity(intent: Intent)
    /** Старт Активити  */
    fun startActivity(action: String)
    /** Старт Активити  */
    fun startActivity(action: String, uri: Uri)
    /** Старт Активити  */
    fun startActivity(activityClass: Class<out Activity>)
    /** Старт Активити  */
    fun startActivity(activityClass: Class<out Activity>, args: Bundle)
    /** Старт Активити  */
    fun startActivity(activityClass: Class<out Activity>, args: Parcelable)
    /** Старт Активити  */
    fun startActivity(activityClass: Class<out Activity>, arg: String)
    /** Старт Активити  */
    fun startActivity(activityClass: Class<out Activity>, arg: Int)

    /** Старт Активити с результатом */
    fun startActivityForResult(activityClass: Class<out Activity>, requestCode: Int)
    /** Старт Активити с результатом */
    fun startActivityForResult(activityClass: Class<out Activity>, arg: Parcelable, requestCode: Int)
    /** Старт Активити с результатом */
    fun startActivityForResult(activityClass: Class<out Activity>, arg: String, requestCode: Int)
    /** Старт Активити с результатом */
    fun startActivityForResult(activityClass: Class<out Activity>, arg: Int, requestCode: Int)

    /** Смена фрагмента */
    fun replaceFragment(@IdRes containerId: Int, fragment: Fragment, args: Bundle)
    /** Смена фрагмента */
    fun replaceFragment(@IdRes containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle)

    /** Смена фрагмента и добавить в стек */
    fun replaceFragmentAndAddToBackStack(@IdRes containerId: Int, fragment: Fragment, args: Bundle, backstackTag: String)
    /** Смена фрагмента и добавить в стек */
    fun replaceFragmentAndAddToBackStack(@IdRes containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle, backstackTag: String)

}