package com.example.gratters.ui.main.viewpager.pagerfragments.all

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.gratters.ui.main.viewpager.IGrattersView
import com.example.gratters.ui.main.viewpager.pagerfragments.GrattersFragment

/**
 * Фрагмент всех поздравлений
 * @author StrangeHare
 *         Date: 21.09.17
 */
class AllGrattersFragment : GrattersFragment<IAllGrattersViewModel>(), IGrattersView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getFragmentComponent()?.inject(this)
    }

    override fun onRefresh(success: Boolean) {
        super.onRefresh(success)
        if (!success) {
            Toast.makeText(context, "error", Toast.LENGTH_LONG).show()
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.swipeRefreshLayout?.setOnRefreshListener { viewModel.reloadData() }
        viewModel.reloadData()
    }
}