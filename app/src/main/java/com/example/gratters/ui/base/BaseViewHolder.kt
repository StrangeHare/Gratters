package com.example.gratters.ui.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.gratters.di.component.DaggerViewHolderComponent
import com.example.gratters.di.component.ViewHolderComponent

/**
 * Главный холдер
 * @author StrangeHare
 *         Date: 22.09.17
 */
abstract class BaseViewHolder<B : ViewDataBinding>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    /** Биндинг */
    var binding: B? = null

    /**
     * Получение компонента холдера
     * @return компонент
     */
    fun getViewHolderComponent(): ViewHolderComponent = DaggerViewHolderComponent.create()

    /**
     * Инициализация биндинга
     * @param View ид макета
     */
    protected fun setAndBindContentView(view: View) {
        binding = DataBindingUtil.bind(view)
    }
}