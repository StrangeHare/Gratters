package com.example.gratters.ui.main.viewpager.pagerfragments.all

import android.os.Bundle
import com.example.gratters.data.Gratter
import com.example.gratters.di.scope.FragmentScope
import com.example.gratters.ui.base.BaseViewModel
import com.example.gratters.ui.main.recyclerview.GrattersRecyclerAdapter
import com.example.gratters.ui.main.viewpager.IGrattersView
import javax.inject.Inject

/**
 * Вью модель всех поздравлений
 *
 * @constructor
 * @param adapter адаптер
 *
 * @author StrangeHare
 *         Date: 26.09.17
 */
@FragmentScope
class AllGrattersViewModel @Inject constructor(
        private val adapter: GrattersRecyclerAdapter
) : BaseViewModel<IGrattersView>(), IAllGrattersViewModel {

    override fun getGrattersAdapter(): GrattersRecyclerAdapter = adapter

    override fun attachView(view: IGrattersView, savedInstanceState: Bundle?) {
        super.attachView(view, savedInstanceState)
    }

    override fun detachView() {
    }

    override fun saveInstanceState(outState: Bundle?) {
    }

    override fun reloadData() {
        adapter.list.clear()
        adapter.list.addAll(arrayListOf(Gratter("fdsfdsf", "dsfdsfdsfdsf"), Gratter("fdsfdsf", "dsfdsfdsfdsf"), Gratter("fdsfdsf", "dsfdsfdsfdsf")))
        adapter.notifyDataSetChanged()
        getView().onRefresh(true)
    }
}