package com.example.gratters.ui.base.viewmodel

import com.example.gratters.ui.base.view.IMvvmView
import com.example.gratters.ui.main.recyclerview.GrattersRecyclerAdapter

/**
 * Интерфейс адаптера вью модели
 * @author StrangeHare
 *         Date: 20.09.17
 */
interface AdapterMvvmViewModel<in V : IMvvmView> : IMvvmViewModel<V> {

    /**
     * Получение адаптера
     * @return адаптер
     */
    fun getGrattersAdapter(): GrattersRecyclerAdapter
}
