package com.example.gratters.ui.main.viewpager.pagerfragments.favorite

import com.example.gratters.ui.base.viewmodel.AdapterMvvmViewModel
import com.example.gratters.ui.main.viewpager.IGrattersView

/**
 * Интерфейс избранных поздравлений
 * @author kurbatov_s
 *         Date: 27.09.17
 */
interface IFavoriteGrattersViewModel : AdapterMvvmViewModel<IGrattersView> {
}