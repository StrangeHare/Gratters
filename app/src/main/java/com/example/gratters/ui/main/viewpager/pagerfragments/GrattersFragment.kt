package com.example.gratters.ui.main.viewpager.pagerfragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.gratters.R
import com.example.gratters.databinding.FragmentGrattersBinding
import com.example.gratters.ui.base.BaseFragment
import com.example.gratters.ui.base.viewmodel.IMvvmViewModel
import com.example.gratters.ui.main.recyclerview.GrattersRecyclerAdapter
import com.example.gratters.ui.main.viewpager.IGrattersView
import javax.inject.Inject

/**
 * Абстрактный фрагмент поздравлений
 * @author StrangeHare
 *         Date: 21.09.17
 */
abstract class GrattersFragment<V : IMvvmViewModel<IGrattersView>> : BaseFragment<FragmentGrattersBinding, V>(), IGrattersView {

    /** Адаптер */
    @Inject
    lateinit var adapterGratters: GrattersRecyclerAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            setAndBindContentView(inflater, container, R.layout.fragment_gratters, savedInstanceState)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.recyclerView.apply {
            this?.setHasFixedSize(true)
            this?.layoutManager = LinearLayoutManager(context)
            this?.adapter = adapterGratters
        }
    }

    override fun onRefresh(success: Boolean) {
        binding?.swipeRefreshLayout?.isRefreshing = false
    }
}