package com.example.gratters.ui.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.gratters.di.component.DaggerFragmentComponent
import com.example.gratters.di.component.FragmentComponent
import com.example.gratters.ui.base.viewmodel.IMvvmViewModel
import com.example.gratters.ui.main.viewpager.IGrattersView
import javax.inject.Inject

/**
 * Главный фрагмент
 * @author StrangeHare
 *         Date: 21.09.17
 */
abstract class BaseFragment<B : ViewDataBinding, V : IMvvmViewModel<IGrattersView>> : Fragment() {

    /** Компонент фрагмента */
    private var fragmentComponent: FragmentComponent? = null
    /** Биндинг */
    var binding: B? = null
    /** Вью модель */
    @Inject
    lateinit var viewModel: V

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        viewModel.saveInstanceState(outState)
    }

    /**
     * Инициализация компонента фрагмента
     * @return компонент
     */
    fun getFragmentComponent(): FragmentComponent? {
        if (fragmentComponent == null) {
            fragmentComponent = DaggerFragmentComponent
                    .builder()
                    .activityComponent((activity as BaseActivity<*, *>).getActivityComponent())
                    .build()
        }

        return fragmentComponent
    }

    /**
     * Инициализация биндинга
     * @param inflater инфлейтер
     * @param parent   куда инфейтить
     * @param res      макет
     */
    protected fun setAndBindContentView(inflater: LayoutInflater?, parent: ViewGroup?, @LayoutRes res: Int, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, res, parent, false)

        try {
            viewModel.attachView(this as IGrattersView, savedInstanceState)
        } catch (e: ClassCastException) {
            throw RuntimeException(javaClass.simpleName + " must implement IMvvmView subclass as declared in " + viewModel.javaClass.simpleName)
        }

        return binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.detachView()
        binding = null
        binding = null
        fragmentComponent = null
    }
}