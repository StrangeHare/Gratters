package com.example.gratters.ui.main.viewpager.pagerfragments.favorite

import android.os.Bundle
import com.example.gratters.di.scope.FragmentScope
import com.example.gratters.ui.base.BaseViewModel
import com.example.gratters.ui.main.recyclerview.GrattersRecyclerAdapter
import com.example.gratters.ui.main.viewpager.IGrattersView
import javax.inject.Inject

/**
 * Вью модель избранных поздравлений
 *
 * @constructor
 * @param adapter адаптер
 *
 * @author StrangeHare
 *         Date: 26.09.17
 */
@FragmentScope
class FavoriteGrattersViewModel @Inject constructor(
        private val adapter: GrattersRecyclerAdapter
) : BaseViewModel<IGrattersView>(), IFavoriteGrattersViewModel {

    override fun getGrattersAdapter(): GrattersRecyclerAdapter = adapter

    override fun attachView(view: IGrattersView, savedInstanceState: Bundle?) {
    }

    override fun detachView() {
    }

    override fun saveInstanceState(outState: Bundle?) {
    }
}