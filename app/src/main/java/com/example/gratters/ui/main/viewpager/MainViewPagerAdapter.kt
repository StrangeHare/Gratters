package com.example.gratters.ui.main.viewpager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.gratters.di.qualifier.ActivityFragmentManagerQualifier
import com.example.gratters.di.scope.ActivityScope
import com.example.gratters.ui.main.viewpager.pagerfragments.favorite.FavoriteGrattersFragment
import com.example.gratters.ui.main.viewpager.pagerfragments.all.AllGrattersFragment
import javax.inject.Inject

/**
 * Адаптер пейджера
 *
 *  @constructor
 *  @param fm фрагмент менеджер
 *
 * @author StrangeHare
 *         Date: 21.09.17
 */
@ActivityScope
class MainViewPagerAdapter @Inject constructor(
        @ActivityFragmentManagerQualifier fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment = if (position == 0) AllGrattersFragment() else FavoriteGrattersFragment()

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence = if (position == 0) "All" else "Favorite"
}