package com.example.gratters.ui.base

import android.os.Bundle
import com.example.gratters.ui.base.view.IMvvmView
import com.example.gratters.ui.base.viewmodel.IMvvmViewModel

/**
 * Базовая вью модель
 * @author StrangeHare
 *         Date: 27.09.17
 */
abstract class BaseViewModel<V : IMvvmView> : IMvvmViewModel<V> {

    /** Вью */
    lateinit var mvvmView: V

    override fun attachView(view: V, savedInstanceState: Bundle?) {
        mvvmView = view
    }

    override fun detachView() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveInstanceState(outState: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Получение вью
     * @return вью
     */
    fun getView(): V = mvvmView
}