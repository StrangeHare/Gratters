package com.example.gratters.ui.main.viewpager.pagerfragments.all

import com.example.gratters.ui.base.viewmodel.AdapterMvvmViewModel
import com.example.gratters.ui.main.viewpager.IGrattersView

/**
 * Интерфейс вью модели поздравлений
 * @author StrangeHare
 *         Date: 26.09.17
 */
interface IAllGrattersViewModel : AdapterMvvmViewModel<IGrattersView> {

    /** Перезагрузка данных */
    fun reloadData()
}