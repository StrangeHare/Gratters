package com.example.gratters.ui.main

import android.os.Bundle
import com.example.gratters.R
import com.example.gratters.databinding.ActivityMainBinding
import com.example.gratters.ui.base.BaseActivity
import com.example.gratters.ui.base.view.IMvvmView
import com.example.gratters.ui.base.viewmodel.IMvvmViewModel
import com.example.gratters.ui.main.viewpager.IGrattersView
import com.example.gratters.ui.main.viewpager.MainViewPagerAdapter
import javax.inject.Inject

/**
 * Главная Активность
 * @author StrangeHare
 * Date: 20.09.17
 */
class MainActivity : BaseActivity<ActivityMainBinding, IMvvmViewModel<IGrattersView>>(), IMvvmView {

    /** Адаптер вью пейджера */
    @Inject
    lateinit var viewPagerAdapter: MainViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivityComponent()?.injectMainActivity(this)
        setAndBindContentView(R.layout.activity_main, savedInstanceState)

        setSupportActionBar(binding?.activityMainToolbar)

        binding?.collapsingToolbar?.isTitleEnabled = false
        binding?.viewPager?.adapter = viewPagerAdapter
        binding?.tabLayout?.setupWithViewPager(binding?.viewPager)
    }
}
