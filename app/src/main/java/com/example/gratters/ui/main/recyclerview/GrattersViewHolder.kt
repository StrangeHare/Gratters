package com.example.gratters.ui.main.recyclerview

import android.view.View
import com.example.gratters.databinding.GrattersViewHolderBinding
import com.example.gratters.ui.base.BaseViewHolder

/**
 * Холдер поздравлений
 * @author StrangeHare
 *         Date: 22.09.17
 */
class GrattersViewHolder(itemView: View): BaseViewHolder<GrattersViewHolderBinding>(itemView) {

    init {
        getViewHolderComponent().injectGrattersViewHolder(this)
        setAndBindContentView(itemView)
    }
}