package com.example.gratters.ui.main.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.gratters.R
import com.example.gratters.data.Gratter
import com.example.gratters.di.scope.FragmentScope
import javax.inject.Inject

/**
 * Адаптер
 * @author StrangeHare
 *         Date: 22.09.17
 */
@FragmentScope
class GrattersRecyclerAdapter @Inject constructor() : RecyclerView.Adapter<GrattersViewHolder>() {

    /** Список поздравлений */
    var list = ArrayList<Gratter>()

    override fun onBindViewHolder(holder: GrattersViewHolder?, position: Int) {
        holder?.binding?.gratter = list[position]
    }

    override fun getItemCount(): Int = list.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GrattersViewHolder =
            GrattersViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.gratters_view_holder, parent, false))
}