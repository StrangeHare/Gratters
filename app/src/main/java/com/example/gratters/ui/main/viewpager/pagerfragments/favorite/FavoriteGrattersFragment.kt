package com.example.gratters.ui.main.viewpager.pagerfragments.favorite

import android.os.Bundle
import android.view.View
import com.example.gratters.ui.main.viewpager.pagerfragments.GrattersFragment

/**
 * Фрагментт избранных поздравлений
 * @author StrangeHare
 *         Date: 21.09.17
 */
class FavoriteGrattersFragment : GrattersFragment<FavoriteGrattersViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getFragmentComponent()?.inject(this)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.swipeRefreshLayout?.setOnRefreshListener { binding?.swipeRefreshLayout?.isRefreshing = false }
    }
}